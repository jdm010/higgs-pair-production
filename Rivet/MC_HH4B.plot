# BEGIN PLOT /MC_HH4B/jet_b_jet_eta
Title= $\eta$ of $b$-jet 
XLabel= $\eta$
YLabel= $\mathrm{d}\sigma/\mathrm{d}\eta$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_b_jet_multiplicity
Title= $b$-jet multiplicity
XLabel= $N_{$b$\mathrm{-jet}}$
YLabel= $\mathrm{d}\sigma/\mathrm{d}N_{b\mathrm{-jet}}$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_b_jet_phi
Title= $\phi$ of $b$-jet
XLabel= $\phi/2\pi$
YLabel= $\mathrm{d}\sigma/\mathrm{d}(\phi/2\pi)$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_b_jet_pT
Title= ${p_T}$ of $b$-jet
XLabel= ${p_T}$ [GeV]
YLabel= $\mathrm{d}\sigma/\mathrm{d}p_T$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_jet_eta
Title= $\eta$ of jet
XLabel= $\eta$
YLabel= $\mathrm{d}\sigma/\mathrm{d}\eta$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_jet_multiplicity
Title= jet multiplicity
XLabel= $N_{jet}$
YLabel= $\mathrm{d}\sigma/\mathrm{d}N_{jet}$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_jet_phi
Title= $\phi$ of jet
XLabel= $\phi/2\pi$
YLabel= $\mathrm{d}\sigma/\mathrm{d}(\phi/2\pi)$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_jet_pT
Title= ${p_T}$ of jet
XLabel= ${p_T}$ [GeV]
YLabel= $\mathrm{d}\sigma/\mathrm{d}p_T$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/jet_jet_mass
Title= mass of jet
XLabel= mass [GeV]
YLabel= $\mathrm{d}\sigma/\mathrm{d}p_T$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/HiggsMass1
Title= 
XLabel= Leading Higgs boson $m_{H} [GeV]$
YLabel= Arbitrary units
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/HiggsMass2
Title= 
XLabel= Subeading Higgs boson $m_{H} [GeV]$
YLabel= Arbitrary units
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/DiHiggsMass
Title= 
XLabel= Invariant di-Higgs $m_{HH} [GeV]$
YLabel= Arbitrary units
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/DiHiggsPt
Title= 
XLabel= $di-Higgs ${p_T} [GeV]$
YLabel= Arbitrary units
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/HiggsPt1
Title= 
XLabel= Leading Higgs boson ${p_T}$ [GeV]
YLabel= Arbitrary units
XMax=800.0
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/HiggsPt2
Title= 
XLabel= Subleading Higgs boson ${p_T}$ [GeV]
YLabel= Arbitrary units
XMax=800.0
LogY=0
# END PLOT

# BEGIN PLOT /MC_HH4B/HiggsMassDiff
Title= 
XLabel= Absolute mass difference between each Higgs candidate $|\Delta m_{H}|$ [GeV]
YLabel= Arbitrary units
LogY=0
# END PLOT

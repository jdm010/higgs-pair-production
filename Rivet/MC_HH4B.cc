// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Math/LorentzTrans.hh"

namespace Rivet {

  class MC_HH4B : public Analysis {
  public:
    /// @name Constructors etc.
    //@{
    /// Constructor
    MC_HH4B()
      : Analysis("MC_HH4B")
    {    }

    //@}
    /// @name Analysis methods
    //@{
    /// Book histograms and initialise projections before the run
    void init() {

      FinalState fs;
      //Cut cut = Cuts::abseta < 3.5 && Cuts::pT > 25*GeV;
      declare(fs, "FinalState");
      declare(FastJets(fs, FastJets::ANTIKT, 0.4), "AntiKT04");
      declare(FastJets(fs, FastJets::ANTIKT, 0.5), "AntiKT05");
      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "AntiKT06");

      /// Book histograms
      book(_h_jet_b_jet_eta ,"jet_b_jet_eta", 50, -4, 4);
      book(_h_jet_b_jet_multiplicity ,"jet_b_jet_multiplicity", 11, -0.5, 10.5);
      book(_h_jet_b_jet_phi ,"jet_b_jet_phi", 50, 0, 1);
      book(_h_jet_b_jet_pT ,"jet_b_jet_pT", 50, 0, 500);
      book(_h_jet_eta ,"jet_eta", 50, -4, 4);
      book(_h_jet_multiplicity ,"jet_multiplicity", 11, -0.5, 10.5);
      book(_h_jet_phi ,"jet_phi", 50, 0, 1);
      book(_h_jet_pT ,"jet_pT", 50, 0, 500);
      book(_h_jet_mass ,"jet_Mass", 50, 0, 500);


      book(_h_HiggsMass1,"HiggsMass1", 50, 0, 250); // Higgs candidate 1 mass
      book(_h_HiggsMass2,"HiggsMass2", 50, 0, 250); // Higgs candidate 2 mass
      book(_h_HiggsPt1,"HiggsPt1", 50, 0, 1000); // Higgs candidate 1 pt
      book(_h_HiggsPt2,"HiggsPt2", 50, 0, 1000); // Higgs candidate 2 pt
      book(_h_DiHiggsMass,"DiHiggsMass", 50, 0, 2000); // Di-Higgs system mass
      book(_h_DiHiggsPt,"DiHiggsPt", 50, 0, 1000); // Di-Higgs system pT
      book(_h_HiggsMassDiff,"HiggsMassDiff", 50, 0, 100); // Di-Higgs system mass
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
      // const double JETPTCUT = 30*GeV;

      const Jets jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pt > 50*GeV && Cuts::abseta < 4.0);
      _h_jet_multiplicity->fill(jets.size());

      // Identify the b-jets
      Jets bjets;
      for (const Jet& jet : jets) {
        const double jetEta = jet.eta();
        const double jetPhi = jet.phi();s
        const double jetPt = jet.pT();
        const double jetMass = jet.mass();

        _h_jet_eta->fill(jetEta);
        _h_jet_phi->fill(jetPhi/2/M_PI);
        _h_jet_pT->fill(jetPt/GeV);
        _h_jet_mass->fill(jetMass/GeV);

        if (jet.bTagged()) {
          bjets.push_back(jet);
          _h_jet_b_jet_eta->fill(jetEta);
          _h_jet_b_jet_phi->fill(jetPhi/2/M_PI);
          _h_jet_b_jet_pT->fill(jetPt);
        }
      }
      _h_jet_b_jet_multiplicity->fill(bjets.size());

      // if(bjets.empty()) vetoEvent;

      if (bjets.size() >= 4) {
      double best_mass_diff = FLT_MAX;
      //double best_deltaR_sum = FLT_MAX;
      std::pair<Jet, Jet> higgs1, higgs2;
    for (size_t i = 0; i < bjets.size(); ++i) {
      for (size_t j = i + 1; j < bjets.size(); ++j) {
        for (size_t k = 0; k < bjets.size(); ++k) {
          if (k == i || k == j) continue;
          for (size_t l = k + 1; l < bjets.size(); ++l) {
            if (l == i || l == j) continue;

            // Calculate masses
            double m1 = (bjets[i].momentum() + bjets[j].momentum()).mass();
            double m2 = (bjets[k].momentum() + bjets[l].momentum()).mass();
            double mass_diff = fabs(m1 - m2);

            bool isOneHiggsInMassRange = (m1 >= 95.0*GeV && m1 <= 145.0*GeV) || (m2 >= 95.0*GeV && m2 <= 145.0*GeV);
            
            // Check for best pairing
            // && mass_diff < 20.0*GeV && isOneHiggsInMassRange
            if (mass_diff < best_mass_diff && mass_diff < 20.0*GeV && isOneHiggsInMassRange) {
              best_mass_diff = mass_diff;
              higgs1 = std::make_pair(bjets[i], bjets[j]);
              higgs2 = std::make_pair(bjets[k], bjets[l]);
            }

          }
        }
      }
    }

            if (best_mass_diff < 20.0*GeV) {
             // Reconstruct Higgs masses
            const FourMomentum higgs_mass1 = higgs1.first.momentum() + higgs1.second.momentum();
            const FourMomentum higgs_mass2 = higgs2.first.momentum() + higgs2.second.momentum();

             bool isOneHiggsInMassRangeFinal = (higgs_mass1.mass() >= 95.0*GeV && higgs_mass1.mass() <= 145.0*GeV) || 
                                           (higgs_mass2.mass() >= 95.0*GeV && higgs_mass2.mass() <= 145.0*GeV);

            double higgs_mass_diff = fabs(higgs_mass1.mass() - higgs_mass2.mass());

            // Fill histograms (redundant mass check)
             if (isOneHiggsInMassRangeFinal) {
            _h_HiggsMass1->fill(higgs_mass1.mass());
            _h_HiggsPt1->fill(higgs_mass1.pT());
            
            _h_HiggsMass2->fill(higgs_mass2.mass());
            _h_HiggsPt2->fill(higgs_mass2.pT());

            _h_HiggsMassDiff->fill(higgs_mass_diff);

             
            // Reconstruct di-Higgs 
            const FourMomentum dihiggsmass = higgs1.first.momentum() + higgs1.second.momentum() +
            higgs2.first.momentum() + higgs2.second.momentum();
            _h_DiHiggsMass->fill(dihiggsmass.mass());
            _h_DiHiggsPt->fill(dihiggsmass.pT());

            }
            }
            }
            
      }
        

    /// Normalise histograms etc., after the run
    void finalize() {
      normalize(_h_jet_b_jet_eta);
      normalize(_h_jet_b_jet_multiplicity);
      normalize(_h_jet_b_jet_phi);
      normalize(_h_jet_b_jet_pT);
      normalize(_h_jet_eta);
      normalize(_h_jet_multiplicity);
      normalize(_h_jet_phi);
      normalize(_h_jet_pT);
      normalize(_h_jet_mass);
      normalize(_h_HiggsMass1);
      normalize(_h_HiggsMass2);
      normalize(_h_HiggsPt1);
      normalize(_h_HiggsPt2);
      normalize(_h_DiHiggsMass);
      normalize(_h_DiHiggsPt);
      normalize(_h_HiggsMassDiff);

      // 

      int jet_events = _h_jet_pT->numEntries();
      int b_jet_events = _h_jet_b_jet_pT->numEntries();
      int four_b_jet_events = _h_HiggsMass1->numEntries();
      
      cout << "Number of events with jets: " << jet_events << endl;
      cout << "Number of events with b-jets: " << b_jet_events << endl;
      cout << "Number of events with 4 b-jets: " << four_b_jet_events << endl;

    }

    //@}



  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_jet_b_jet_eta, _h_jet_b_jet_multiplicity, _h_jet_b_jet_phi, _h_jet_b_jet_pT;
    Histo1DPtr _h_jet_eta, _h_jet_multiplicity, _h_jet_phi, _h_jet_pT; 
    Histo1DPtr _h_jet_mass;
    Histo1DPtr _h_HiggsMass1, _h_HiggsMass2, _h_DiHiggsMass;
    Histo1DPtr _h_DiHiggsPt;
    Histo1DPtr _h_HiggsPt1;
    Histo1DPtr _h_HiggsPt2;
    Histo1DPtr _h_HiggsMassDiff;
    //Histo1DPtr _h_jet_cuts_bb_deltaR_v_HpT;

    //@}

  };

  // This global object acts as a hook for the plugin system
  RIVET_DECLARE_PLUGIN(MC_HH4B);

}
